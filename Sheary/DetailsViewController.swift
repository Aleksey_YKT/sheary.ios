//
//  DetailViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 26.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    @IBOutlet weak var barbershopImageView: UIImageView!
    @IBOutlet weak var barbershopDescriptionView: UITextView!
    @IBOutlet weak var barbershopNameTitle: UINavigationItem!
    @IBOutlet weak var servicesButton: UIButton!
    @IBOutlet weak var barbershopAddressLabel: UILabel!
    @IBOutlet weak var barbershopDistanceLabel: UILabel!
    @IBOutlet weak var barbershopPhoneLabel: UILabel!
    
    var barbershop = Barbershop()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageUrl = siteUrl + barbershop.image
        
        barbershopNameTitle.title = barbershop.name
        
        barbershopDescriptionView.text = barbershop.description
        
        if let checkedUrl = URL(string: imageUrl) {
            barbershopImageView.contentMode = .scaleAspectFill
            downloadImage(url: checkedUrl)
        }
        
        barbershopAddressLabel.text = barbershop.address
        
        // Вывод расстояния до салона (если расстояние меньше 1 км, то вывод метрах)
        if (barbershop.distance > 1) {
            barbershopDistanceLabel.text = String(format:"%.1f", barbershop.distance) + " км"
        } else {
            barbershopDistanceLabel.text = String(format:"%.1f", barbershop.distance * 1000) + " м"
        }
        
        barbershopPhoneLabel.text = barbershop.phone
    }
    
    override func viewDidAppear(_ animated: Bool) {
        barbershopDescriptionView.contentOffset = CGPoint.zero
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Открытие следующего view - ServicesTableViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showBarbershopServices" {
            let destinationController = segue.destination as! ServicesTableViewController
            destinationController.barbershop = barbershop
        }
    }
    
    // Загрузка изображений из сервера
    func downloadImage(url: URL) {
        getDataFromUrl(url: url) { (data, response, error)  in guard let data = data, error == nil else { return }
            DispatchQueue.main.async() { () -> Void in
                self.barbershopImageView.image = UIImage(data: data)
            }
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (_ data: Data?, _  response: URLResponse?, _ error: Error?) -> Void) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            completion(data, response, error)
            }.resume()
    }
}
