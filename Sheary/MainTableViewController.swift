//
//  TableViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 24.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit
import CoreLocation

class MainTableViewController: UITableViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var lonCoordinate = 0.0
    var latCoordinate = 0.0
    
    var barbershop = [Barbershop]()
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        definesPresentationContext = true

        // Получения разрешения на использование геолокации
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        // Реализация обновления
        self.refreshControl?.addTarget(self, action: #selector(MainTableViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
        
        // Получение данных из сервера
        getDataFromServer()
    }
    
    // Обновление таблицы
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        barbershop = []
        getDataFromServer()
        
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }

    @IBAction func BackToMainView(segue: UIStoryboardSegue) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return barbershop.count
    }
    
    // Заполнение ячеек
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell", for: indexPath) as! MainTableViewCell
        
        cell.nameLabel?.text = barbershop[(indexPath as NSIndexPath).row].name
        cell.addressLabel?.text = barbershop[(indexPath as NSIndexPath).row].address

        if (barbershop[(indexPath as NSIndexPath).row].distance > 1) {
            cell.distanceLabel?.text = String(format:"%.1f", barbershop[(indexPath as NSIndexPath).row].distance) + " км"
        } else {
            cell.distanceLabel?.text = String(format:"%.1f", barbershop[(indexPath as NSIndexPath).row].distance * 1000) + " м"
        }

        return cell
    }
    
    // Открытие следующего view - DetailsViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showBarbershopDetails" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! DetailsViewController
                destinationController.barbershop = barbershop[(indexPath as NSIndexPath).row]
            }
        }
    }
    
    // Получение JSON файла из сервера
    func getDataFromServer() {
        let getBarbershopsRequestURLWithCoords = getBarbershopsRequestURL + "?lat=" + String(latCoordinate) + "&lon=" + String(lonCoordinate)
        
        let request = URLRequest(url: URL(string: getBarbershopsRequestURLWithCoords)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseBarbershopsData(data)
                
                OperationQueue.main.addOperation({ () -> Void in
                    self.tableView.reloadData()
                })
            }
            
        })
        task.resume()
    }

    // Парсинг полученной JSON
    func parseBarbershopsData(_ data: Data) {
        do {
            guard let barbershopsData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert barbershops to JSON")
                return
            }
            
            for barbershopData in barbershopsData {
                let temp_barbershop = Barbershop()
                
                temp_barbershop.id = barbershopData["id"] as! Int
                temp_barbershop.name = barbershopData["name"] as! String
                temp_barbershop.description = barbershopData["description"] as! String
                temp_barbershop.address = barbershopData["address"] as! String
                temp_barbershop.phone = barbershopData["phone"] as! String
                temp_barbershop.lon = barbershopData["lon"] as! Float
                temp_barbershop.lat = barbershopData["lat"] as! Float
                temp_barbershop.image = barbershopData["image"] as! String
                temp_barbershop.distance = barbershopData["distance"] as! Float
                
                barbershop.append(temp_barbershop)
            }
            
        } catch {
            print(error)
        }
    }
    
    // Определение координат пользователя
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        lonCoordinate = locValue.longitude
        latCoordinate = locValue.latitude
    }
    
}
