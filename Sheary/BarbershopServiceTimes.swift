//
//  ServiceTimes.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 26.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

class BarbershopServiceTimes {
    var id: Int = 0
    var time: String = ""
    var available: Bool = false
}
