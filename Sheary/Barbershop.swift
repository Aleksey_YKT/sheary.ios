//
//  Barbershop.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 24.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

class Barbershop {
    var id: Int = 0
    var name: String = ""
    var description: String = ""
    var address: String = ""
    var phone: String = ""
    var lon: Float = 0
    var lat: Float = 0
    var image: String = ""
    var distance: Float = 0
}
