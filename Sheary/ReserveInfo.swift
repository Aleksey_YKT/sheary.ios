//
//  ReserveInfo.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 28.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

class ReserveInfo {
    var barbershop_id: Int = 0
    var service_id: Int = 0
    var time_id: Int = 0
    var confirmed: Bool = false
}
