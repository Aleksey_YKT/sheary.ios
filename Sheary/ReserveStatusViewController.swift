//
//  ReserveStatusViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 27.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class ReserveStatusViewController: UIViewController {

    @IBOutlet weak var barbershopNameLabel: UILabel!
    @IBOutlet weak var barbershopAddressLabel: UILabel!
    @IBOutlet weak var barbershopPhoneLabel: UILabel!
    @IBOutlet weak var barbershopServiceLabel: UILabel!
    @IBOutlet weak var barbershopServiceTimeLabel: UILabel!
    @IBOutlet weak var barbershopServicePriceLabel: UILabel!
    @IBOutlet weak var serviceStatus: UILabel!
    
    var barbershop = Barbershop()
    var barbershopService = BarbershopServices()
    var barbershopServiceTime = BarbershopServiceTimes()
    var reservesInfo = ReserveInfo()
    var reserveStatus = ""
    
    var needToHideBackButtom = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Скрываем кнопку назад, чтобы нельзя было вернуться и посылать один и тот же заказ несколько раз
        if (needToHideBackButtom) {
            self.navigationItem.setHidesBackButton(true, animated:true)
        }
        
        barbershopNameLabel.text = barbershop.name
        barbershopAddressLabel.text = barbershop.address
        barbershopPhoneLabel.text = barbershop.phone
        barbershopServiceLabel.text = barbershopService.name
        barbershopServiceTimeLabel.text = CustomDateFormatter(serviceTime: barbershopServiceTime.time)
        barbershopServicePriceLabel.text = String(barbershopService.price) + " руб."
        
        if (reservesInfo.confirmed) {
            serviceStatus.text = "Подтвержден"
        } else {
            serviceStatus.text = "Не подтвержден"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
