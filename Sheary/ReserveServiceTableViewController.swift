//
//  ReserveServiceTableViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 26.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class ReserveServiceTableViewController: UITableViewController {

    var barbershop = Barbershop()
    var barbershopService = BarbershopServices()
    var barbershopServiceTimes = [BarbershopServiceTimes]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBarbershopsServiceTimesData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return barbershopServiceTimes.count
    }
    
    // Заполнение ячеек
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reserveTimeCell", for: indexPath) as! ReserveServiceTableViewCell
        
        cell.serviceTimeLabel?.text = CustomDateFormatter(serviceTime: barbershopServiceTimes[(indexPath as NSIndexPath).row].time)
        
        return cell
    }
    
    // Открытие следующего view - ConfirmViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showConfirmView" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! ConfirmViewController
                destinationController.barbershop = barbershop
                destinationController.barbershopService = barbershopService
                destinationController.barbershopServiceTime = barbershopServiceTimes[(indexPath as NSIndexPath).row]
            }
        }
    }
    
    // Получение JSON файла из сервера
    func getBarbershopsServiceTimesData() {
        // Формирование GET запроса для API
        let getBarbershopsServiceTimesRequestURLWithId = getBarbershopsServiceTimesRequestURL + "?id=" + String(barbershopService.id)
        
        let request = URLRequest(url: URL(string: getBarbershopsServiceTimesRequestURLWithId)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseBarbershopsServicesData(data)
                
                OperationQueue.main.addOperation({ () -> Void in
                    self.tableView.reloadData()
                })
            }
            
        })
        task.resume()
    }
    
    // Парсинг полученной JSON
    func parseBarbershopsServicesData(_ data: Data) {
        do {
            guard let barbershopsServiceTimesData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert data to JSON")
                return
            }
            
            for barbershopServiceTimeData in barbershopsServiceTimesData {
                let serviceTime = BarbershopServiceTimes()
                
                serviceTime.id = barbershopServiceTimeData["id"] as! Int
                serviceTime.time = barbershopServiceTimeData["time"] as! String
                serviceTime.available = barbershopServiceTimeData["available"] as! Bool
    
                if (serviceTime.available) {
                    barbershopServiceTimes.append(serviceTime)
                }
            }
        } catch {
            print(error)
        }
    }

}
