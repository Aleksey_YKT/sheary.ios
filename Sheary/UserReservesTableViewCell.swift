//
//  UserReservesTableViewCell.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 28.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class UserReservesTableViewCell: UITableViewCell {
    
    @IBOutlet weak var barbeshopNameLabel: UILabel!
    @IBOutlet weak var serviceTimeClockLabel: UILabel!
    @IBOutlet weak var serviceTimeDataLabel: UILabel!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var barbershopAddressLabel: UILabel!

    override func awakeFromNib() {

        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
