//
//  ServicesTableViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 26.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class ServicesTableViewController: UITableViewController {

    var barbershop = Barbershop()
    var barbershopServices = [BarbershopServices]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getBarbershopsServicesData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return barbershopServices.count
    }
    
    // Заполнение ячеек таблицы
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "servicesTableCell", for: indexPath) as! ServicesTableViewCell
        
        cell.serviceNameLabel?.text = barbershopServices[(indexPath as NSIndexPath).row].name
        cell.servicePriceLabel?.text = "\(barbershopServices[(indexPath as NSIndexPath).row].price)" + " руб"
        
        return cell
    }
    
    // Открытие слеующего view - ReserveServiceTableViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showServiceTimes" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! ReserveServiceTableViewController
                destinationController.barbershop = barbershop
                destinationController.barbershopService = barbershopServices[(indexPath as NSIndexPath).row]
            }
        }
    }

    // Получение JSON файла из сервера
    func getBarbershopsServicesData() {
        // Формирование GET запроса к API
        let getBarbershopsServicesRequestURLWithId = getBarbershopsServicesRequestURL + "?id=" + String(barbershop.id)
        
        let request = URLRequest(url: URL(string: getBarbershopsServicesRequestURLWithId)!)
        let urlSession = URLSession.shared
        let task = urlSession.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            
            if let error = error {
                print(error)
                return
            }
            
            if let data = data {
                self.parseBarbershopsServicesData(data)
                
                OperationQueue.main.addOperation({ () -> Void in
                    self.tableView.reloadData()
                })
            }
            
        })
        task.resume()
    }
    
    // Парсинг полученной JSON
    func parseBarbershopsServicesData(_ data: Data) {
        do {
            guard let barbershopsServicesData = try JSONSerialization.jsonObject(with: data, options: []) as? [NSDictionary] else {
                print("error trying to convert data to JSON")
                return
            }
            
            for barbershopServiceData in barbershopsServicesData {
                let service = BarbershopServices()
                
                service.id = barbershopServiceData["id"] as! Int
                service.name = barbershopServiceData["name"] as! String
                service.price = barbershopServiceData["price"] as! Int
                
                barbershopServices.append(service)
            }
        } catch {
            print(error)
        }
    }

}
