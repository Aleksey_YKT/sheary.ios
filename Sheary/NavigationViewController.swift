//
//  ViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 26.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationBar.barTintColor = UIColor(red: 0.0/255.0, green: 128.0/255.0, blue: 205.0/255.0, alpha: 1.0)
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
