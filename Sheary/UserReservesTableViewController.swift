//
//  UserReservesTableViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 28.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class OperationNums {
    static let GET_RESERVES_INFO_OPERATION = 0
    static let GET_BARBERSHOP_OPERATION = 1
    static let GET_BARBERSHOP_SERVICE_OPERATION = 2
    static let GET_BARBERSHOP_TIME_OPERTAION = 3
}

class UserReservesTableViewController: UITableViewController {
    
    var barbershopArray = [Barbershop]()
    var barbershopServiceArray = [BarbershopServices]()
    var barbershopServiceTimeArray = [BarbershopServiceTimes]()
    
    var reservesList = [Int]()
    var reservesInfo = [ReserveInfo]()
    
    var ReadyToShow = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUserReserves()
        
        self.refreshControl?.addTarget(self, action: #selector(UserReservesTableViewController.handleRefresh(_:)), for: UIControlEvents.valueChanged)
    }
    
    // Перезагрузка данных из сервера
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        barbershopArray = []
        barbershopServiceArray = []
        barbershopServiceTimeArray = []
        
        reservesInfo = []
        reservesList = []
        
        ReadyToShow = false
        
        updateUserReserves()
    }
    
    // Чтение id заказов пользователя из файла и выгрузка по ним данных о заказе
    func updateUserReserves() {
        let userReservesString = loadDataFromFile()
        reservesList = parseJSONFromString(JSONString: userReservesString)
        
        for reserveID in reservesList {
            OperationQueue.main.isSuspended = true
            getDataFromServer(getOperationURL: getReserveInfoURL, id: reserveID, getOperationNum: OperationNums.GET_RESERVES_INFO_OPERATION)
        }
    }
    
    // Выгрузка полной информации о заказе
    func updateBarbershopDetails() {
        for reserve in reservesInfo {
            getDataFromServer(getOperationURL: getBarbershopURL, id: reserve.barbershop_id, getOperationNum: OperationNums.GET_BARBERSHOP_OPERATION)
            getDataFromServer(getOperationURL: getBarbershopServiceURL, id: reserve.service_id, getOperationNum: OperationNums.GET_BARBERSHOP_SERVICE_OPERATION)
            getDataFromServer(getOperationURL: getBarbershopServiceTimeURL, id: reserve.time_id, getOperationNum: OperationNums.GET_BARBERSHOP_TIME_OPERTAION)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return reservesInfo.count
    }
    
    // Заполнение ячеек таблицы
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "userReservesCell", for: indexPath) as! UserReservesTableViewCell
        
        cell.barbeshopNameLabel?.text = barbershopArray[(indexPath as NSIndexPath).row].name
        cell.barbershopAddressLabel?.text = barbershopArray[(indexPath as NSIndexPath).row].address
        cell.serviceNameLabel?.text = barbershopServiceArray[(indexPath as NSIndexPath).row].name
        
        cell.serviceTimeDataLabel?.text = CustomDateFormatterDate(serviceTime: barbershopServiceTimeArray[(indexPath as NSIndexPath).row].time)
        cell.serviceTimeClockLabel?.text = CustomDateFormatterClock(serviceTime: barbershopServiceTimeArray[(indexPath as NSIndexPath).row].time)
        
        return cell
    }
    
    // Открытие следующего view - ReserveStatusViewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReserveStatus" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! ReserveStatusViewController
                destinationController.barbershop = barbershopArray[(indexPath as NSIndexPath).row]
                destinationController.barbershopService = barbershopServiceArray[(indexPath as NSIndexPath).row]
                destinationController.barbershopServiceTime = barbershopServiceTimeArray[(indexPath as NSIndexPath).row]
                destinationController.reservesInfo = reservesInfo[(indexPath as NSIndexPath).row]
                destinationController.needToHideBackButtom = false
            }
        }
    }
    
    // Получение JSON файла из сервера
    func getDataFromServer(getOperationURL: String, id: Int, getOperationNum: Int) {
        let getOperationURLWithID = getOperationURL + "?id=" + String(id)
        let request = URLRequest(url: URL(string: getOperationURLWithID)!)
        
        let task = URLSession.shared.dataTask(with: request, completionHandler:  { (data, response, error) -> Void in
            if let data = data {
                let requestAnswer = String(data: data, encoding: .utf8)!
                
                switch(getOperationNum) {
                case OperationNums.GET_RESERVES_INFO_OPERATION:
                    let temp = self.parseReservesInfoJSON(requestAnswer)
                    self.reservesInfo.append(temp)
                    
                    if (self.reservesInfo.count == self.reservesList.count) {
                        self.updateBarbershopDetails()
                    }
                case OperationNums.GET_BARBERSHOP_OPERATION:
                    let temp = self.parseBarbershopJSON(requestAnswer)
                    self.barbershopArray.append(temp)
                    
                    self.checkForReadyToShow()
                case OperationNums.GET_BARBERSHOP_SERVICE_OPERATION:
                    let temp = self.parseBarbershopServiceJSON(requestAnswer)
                    self.barbershopServiceArray.append(temp)
                    
                    self.checkForReadyToShow()
                case OperationNums.GET_BARBERSHOP_TIME_OPERTAION:
                    let temp = self.parseBarbershopServiceTimeJSON(requestAnswer)
                    self.barbershopServiceTimeArray.append(temp)
                    
                    self.checkForReadyToShow()
                default:
                    return
                }
            }
            
            if let error = error {
                print(error)
            }
        })
        
        task.resume()
    }
    
    // Проверка на окончание загрузки и вывод ячеек таблицы
    func checkForReadyToShow() {
        if (self.barbershopArray.count == self.reservesInfo.count) {
            self.ReadyToShow = true
            
            if (self.barbershopServiceArray.count == self.reservesInfo.count) {
                self.ReadyToShow = true
                
                if (self.barbershopServiceTimeArray.count == self.reservesInfo.count) {
                    self.ReadyToShow = true
                } else {
                    self.ReadyToShow = false
                }
            } else {
                self.ReadyToShow = false
            }
        } else {
            self.ReadyToShow = false
        }
    
        if (self.ReadyToShow) {
            OperationQueue.main.addOperation({ () -> Void in
                self.tableView.reloadData()
                self.refreshControl?.endRefreshing()
            })
        }
    }
    
    // Парсинг полученной JSON для бронирований
    func parseReservesInfoJSON(_ dataString: String) -> ReserveInfo {
        let tempReserve = ReserveInfo()
        let data = dataString.data(using: .utf8)
        
        do {
            guard let reserveData = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary else {
                print("error trying to convert data to JSON")
                return tempReserve
            }
            
            tempReserve.barbershop_id = reserveData["barbershop_id"] as! Int
            tempReserve.service_id = reserveData["service_id"] as! Int
            tempReserve.time_id = reserveData["time_id"] as! Int
            tempReserve.confirmed = reserveData["confirmed"] as! Bool
        } catch {
            print(error)
        }
        
        return tempReserve
    }
    
    // Парсинг полученной JSON для салонов
    func parseBarbershopJSON(_ dataString: String) -> Barbershop {
        let barbershop = Barbershop()
        let data = dataString.data(using: .utf8)
            
        do {
            guard let barbershopData = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary else {
                print("error trying to convert data to JSON")
                return barbershop
            }
            
            barbershop.id = barbershopData["id"] as! Int
            barbershop.name = barbershopData["name"] as! String
            barbershop.description = barbershopData["description"] as! String
            barbershop.address = barbershopData["address"] as! String
            barbershop.phone = barbershopData["phone"] as! String
            barbershop.lon = barbershopData["lon"] as! Float
            barbershop.lat = barbershopData["lat"] as! Float
            barbershop.image = barbershopData["image"] as! String
        } catch {
            print(error)
        }
        
        return barbershop
    }
    
    // Парсинг полученной JSON для услуг салонов
    func parseBarbershopServiceJSON(_ dataString: String) -> BarbershopServices {
        let barbershopService = BarbershopServices()
        let data = dataString.data(using: .utf8)
        
        do {
            guard let barbershopServiceData = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary else {
                print("error trying to convert data to JSON")
                return barbershopService
            }
            
            barbershopService.id = barbershopServiceData["id"] as! Int
            barbershopService.name = barbershopServiceData["name"] as! String
            barbershopService.price = barbershopServiceData["price"] as! Int
        } catch {
            print(error)
        }
        
        return barbershopService
    }
    
    // Парсинг полученной JSON для времен представления услуги салоном
    func parseBarbershopServiceTimeJSON(_ dataString: String) -> BarbershopServiceTimes {
        let barbershopServiceTime = BarbershopServiceTimes()
        let data = dataString.data(using: .utf8)
        
        do {
            guard let barbershopServiceTimeData = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary else {
                print("error trying to convert data to JSON")
                return barbershopServiceTime
            }
            
            barbershopServiceTime.id = barbershopServiceTimeData["id"] as! Int
            barbershopServiceTime.time = barbershopServiceTimeData["time"] as! String
            barbershopServiceTime.available = barbershopServiceTimeData["available"] as! Bool
        } catch {
            print(error)
        }
        
        return barbershopServiceTime
    }
    
    // Загрузка id бронирований пользователя из файла
    func loadDataFromFile() -> String {
        var userReservesStringFromFile = ""
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let path = dir.appendingPathComponent(userReservesFile)
            
            //reading
            do {
                userReservesStringFromFile = try String(contentsOf: path, encoding: String.Encoding.utf8)
            }
            catch {
                print("Ошибка при чтении с файла")
            }
        }
        
        return userReservesStringFromFile
    }
    
    // Парсинг JSON полученного из файла
    func parseJSONFromString(JSONString: String) -> [Int] {
        var userReservesIdList = [Int]()
        
        let JSONData = JSONString.data(using: .utf8)
        
        do {
            let json = try JSONSerialization.jsonObject(with: JSONData!, options: .allowFragments)
            
            for blog in json as! [[String: AnyObject]] {
                if let id = blog["id"] as? Int {
                    userReservesIdList.append(id)
                }
            }
        } catch {
            print("error serializing JSON: \(error)")
        }
        
        return userReservesIdList
    }
}
