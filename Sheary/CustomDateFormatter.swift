//
//  DateFormatter.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 27.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

func CustomDateFormatter(serviceTime: String) -> String {
    var formattedServiceTime = String()
    
    if (serviceTime.characters.count > 0) {
        let dateString = serviceTime
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ru_RU_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "d MMMM HH:mm"
        formattedServiceTime = dateFormatter.string(from: dateObj!)
    }
    
    return formattedServiceTime
}

func CustomDateFormatterDate(serviceTime: String) -> String {
    var formattedServiceTime = String()
    
    if (serviceTime.characters.count > 0) {
        let dateString = serviceTime
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ru_RU_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "dd.MM"
        formattedServiceTime = dateFormatter.string(from: dateObj!)
    }
    
    return formattedServiceTime
}

func CustomDateFormatterClock(serviceTime: String) -> String {
    var formattedServiceTime = String()
    
    if (serviceTime.characters.count > 0) {
        let dateString = serviceTime
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale(localeIdentifier: "ru_RU_POSIX") as Locale!
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        let dateObj = dateFormatter.date(from: dateString)
        dateFormatter.dateFormat = "HH:mm"
        formattedServiceTime = dateFormatter.string(from: dateObj!)
    }
    
    return formattedServiceTime
}
