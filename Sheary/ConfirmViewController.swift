//
//  ConfirmViewController.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 26.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import UIKit

class ConfirmViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var barbershopNameLabel: UILabel!
    @IBOutlet weak var barbershopServiceLabel: UILabel!
    @IBOutlet weak var barbershopServiceTimeLabel: UILabel!
    @IBOutlet weak var barbershopServicePriceLabel: UILabel!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var userPhoneNumberTextField: UITextField!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var barbershop = Barbershop()
    var barbershopService = BarbershopServices()
    var barbershopServiceTime = BarbershopServiceTimes()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        barbershopNameLabel.text = barbershop.name
        barbershopServiceLabel.text = barbershopService.name
        barbershopServiceTimeLabel.text = String(CustomDateFormatter(serviceTime: barbershopServiceTime.time))
        barbershopServicePriceLabel.text = String(barbershopService.price) + " руб"
        
        // Do any additional setup after loading the view.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ConfirmViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    // Небольшой скроллинг вверх при открытии клавиатуры
    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 100), animated: true)
    }
    
    // Возвращение в обратное положение
    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: -60), animated: true)
    }
    
    @IBAction func confirmButtonPressed(sender: AnyObject) {
        view.endEditing(true)
        
        let userName = userNameTextField.text!
        let userPhoneNumber = userPhoneNumberTextField.text!
        
        if (userName != "" && userPhoneNumber != "") {
            reservePostRequest(userName: userName, userPhoneNumber: userPhoneNumber)
        } else {
            let alertMessage = UIAlertController(title: "Ошибка", message: "Заполните поля", preferredStyle: .alert)
            alertMessage.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertMessage, animated: true, completion: nil)
        }
    }
    
    // POST запрос с отправкой данных пользователя и сохранение номера id бронирования в файл
    func reservePostRequest(userName: String, userPhoneNumber: String) {
        // Формирование POST запроса для API
        let requestParams = "name=" + userName + "&phone=" + userPhoneNumber + "&time_id=" + String(barbershopServiceTime.id)
            
        var request = URLRequest(url: URL(string: setBarbershopReserveURL)!)
        request.httpMethod = "POST"
        let postString = requestParams
        request.httpBody = postString.data(using: .utf8)
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {
                // check for fundamental networking error
                print("Ошибка на сервере \(error)")
                    
                return
            }
                
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                // check for http errors
                print("Ошибка на сервере \(httpStatus.statusCode)")
            }
            
            print("Ваш запрос отправлен")
            var responseString = String(data: data, encoding: .utf8)
            print(responseString!)
            
            // Сохранение в файл
            if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                let path = dir.appendingPathComponent(userReservesFile)
                var originalText = ""
                
                // Получение предыдущих данных и добавление к ним нового
                do {
                    originalText = try String(contentsOf: path, encoding: String.Encoding.utf8)
                } catch {
                    print("Ошибка при чтении с файла")
                }
                
                if (originalText.characters.count > 0) {
                    // Удаление символа '[' в начале строки
                    originalText.remove(at: originalText.startIndex)
                    
                    // Удаление символа ']' в конце строки
                    originalText.remove(at: originalText.index(before: originalText.endIndex))
                    
                    // Добавление нового id бронирования
                    responseString = "[" + originalText + ", " + responseString! + "]"
                } else {
                    responseString = "[" + responseString! + "]"
                }
                
                // Сохранение в файл
                do {
                    try responseString!.write(to: path, atomically: false, encoding: String.Encoding.utf8)
                } catch {
                    print("Ошибка при сохранении в файл")
                }
            }
        }
        
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReserveStatusView" {
            let destinationController = segue.destination as! ReserveStatusViewController
            
            destinationController.barbershop = barbershop
            destinationController.barbershopService = barbershopService
            destinationController.barbershopServiceTime = barbershopServiceTime
            destinationController.needToHideBackButtom = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
