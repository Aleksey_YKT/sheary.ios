//
//  GlobalVariables.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 28.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

public let userReservesFile = "user_reserves.txt"
public let barbershopMasterToken = "master_token.txt"

public let siteUrl = "http://sheary.ru"

public let getBarbershopsRequestURL = siteUrl + "/api/get_nearest_barbershops/"
public let getBarbershopsServicesRequestURL = siteUrl + "/api/get_barbershop_services/"
public let getBarbershopsServiceTimesRequestURL = siteUrl + "/api/get_barbershop_times/"
public let setBarbershopReserveURL = siteUrl + "/api/set_barbershop_reserve/"

public let getReserveInfoURL = siteUrl + "/api/get_reserve_info/"
public let getBarbershopURL = siteUrl + "/api/get_barbershop/"
public let getBarbershopServiceURL = siteUrl + "/api/get_barbershop_service/"
public let getBarbershopServiceTimeURL = siteUrl + "/api/get_barbershop_time/"
