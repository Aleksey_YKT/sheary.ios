//
//  BarbershopServices.swift
//  Sheary
//
//  Created by Aleksey Ivanov on 26.10.16.
//  Copyright © 2016 Hutec llc. All rights reserved.
//

import Foundation

class BarbershopServices {
    var id: Int = 0
    var name: String = ""
    var price: Int = 0
}
